@extends('dashboard.app')

@section('content')
<div class="container">
    @if (count($pads) == 0)
        <div class="alert alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">x</span>
        </button>
        You have no pads yet, why don't you create your first one!</div>
        <div class="row boards-holder">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="panel panel-default add-new-board">
                    <div class="panel-heading">Add a Board!<span class="pull-right"><small></small></span></div>

                    <div class="panel-body center">
                        <i class="fa fa-plus-circle fa-3x pull-center" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
    @else
    <div class="row boards-holder">
        @foreach ($pads as $pad)
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="panel panel-default" data-guid="{{ $pad->guid }}">
                <div class="panel-heading">{{ $pad->title }} <span class="pull-right"></span></div>

                <div class="panel-body">
                    {{ $pad-> body }} 
                    <div class="circle">
                        {{ count($pad->cards) }}
                    </div>
                </div>
                <div class="panel-date panel-date-extra-margin">{{ getDateFromDateTime($pad->created_at) }}</div>
            </div>
        </div>
        @endforeach

        <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="panel panel-default add-new-board">
                    <div class="panel-heading">Add a Board!<span class="pull-right"><small></small></span></div>

                    <div class="panel-body center">
                        <i class="fa fa-plus-circle fa-2x pull-center" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@endsection
