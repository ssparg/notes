<div id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <li class="sidebar-brand">
            <a href="#">
                {{ Auth::user()->name }}'s Stuff
            </a>
        </li>
        <li>
            <a href="#">Pads</a>
        </li>
        <li>
            <a href="#">Cards</a>
        </li>
        <li>
            <a href="#">Notes</a>
        </li>
        <li>
            <a href="#">Logout</a>
        </li>
    </ul>
</div>