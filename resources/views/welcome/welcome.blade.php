@extends('welcome.app')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 style="text-align: center;">Noter</h1>
        <p class="lead" style="text-align: center;">for your love of notes</p>
    </div>
</div>
<div class="row">
    <div class="col-lg-12" style="text-align: center;">
        <a class="btn btn-default" href="{{ url('/login') }}"><i class="fa fa-sign-in"></i> Login</a>
        <a class="btn btn-default" href="{{ url('/register') }}"><i class="fa fa-user"></i> Register</a>
    </div>
</div>
@stop
    