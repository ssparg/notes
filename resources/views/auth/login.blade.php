@extends('welcome.app')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 style="text-align: center;">Noter</h1>
        <p class="lead" style="text-align: center;">for your love of notes</p>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
        {{ csrf_field() }}
          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email">Email address</label>
            <input type="email" class="form-control" name="email" placeholder="Email">
          </div>
          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" name="password" placeholder="Password">
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox"> Remember me
            </label>
          </div>
          <button type="submit" class="btn btn-default">Submit</button>
          <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
        </form>
    </div>
</div>
@stop