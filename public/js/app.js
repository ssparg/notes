class Pad {

	listen() {
		$(".add-new-board").on("click", this.addNewPad);
		$(".row").on("click", ".fa-pencil", this.editInputs);
		$("html").on("click", this.dirtyHandler);
	}

	addNewPad() {
		var dt = new Date();
		var month = dt.getMonth() + 1;
		if (month < 10) {
			month = '0' + month;
		}
		var currentDate = dt.getFullYear() + "-" + month + "-" + dt.getDate();
		$(".boards-holder").prepend('<div class="col-lg-4 col-md-6 col-sm-12"><div class="panel panel-default"><div class="panel-heading"><input type="text" class="pad-inputs title" value="A new Board!" disabled /><i class="fa fa-pencil pull-right" aria-hidden="true"></i></div><div class="panel-body"><input type="text" class="pad-inputs body marg" maxlength="78" disabled value="Short Description..." /><i class="fa fa-pencil pull-right" aria-hidden="true"></i></div><div class="panel-date">' + currentDate + '</div></div></div>');
	}

	editInputs(e) {
		$(this).parent().find(".pad-inputs").removeAttr('disabled').select();
		$(this).closest('.panel-default').addClass('dirty');
	}

	dirtyHandler(e) {
		var items = [];
		
		if (($(".dirty").length > 0) && !$(e.target).is('.panel *')) {
			// Lets grab all boards and insert/update db.
			$(".dirty").each(function() {
				var temp = {};
				$(this).children().find('.pad-inputs').each(function() {
					
					if ($(this).hasClass("title")) {
						temp["title"] = $(this).val().trim();
					} else {
						temp["body"] = $(this).val().trim();
					}
				});

				items.push(temp);
				
			});

			// Now we have all the objects lets post em!
			
			$.ajax({
				url: "pad",
				method: "POST",
				data: JSON.stringify(items),
				contentType: "application/json; charset=utf-8",
				success: function(data) {
					$(".toast").html(data.message);
					$(".toast").slideDown("slow", function() {
		                setTimeout(function() {
		                    $(".toast").slideUp();
		                }, 2000);
            		});
				},
				error: function(data) {
					var response = JSON.parse(data.responseText)
					$(".toast").html(response.message);
					$(".toast").slideDown("slow", function() {
		                setTimeout(function() {
		                    $(".toast").slideUp();
		                }, 2000);
            		});
				}
			});
			
			// Removing the dirty class
			$(".panel").removeClass("dirty");
		}
	}
}

add = new Pad();
add.listen(); 
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
//# sourceMappingURL=app.js.map
