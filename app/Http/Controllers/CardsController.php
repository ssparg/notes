<?php

namespace Noter\Http\Controllers;

use Illuminate\Http\Request;
use Noter\Http\Requests;
use Noter\Card;
use Noter\User;

class CardsController extends Controller
{
    public function index(Card $card)
    {
    	return $card->notes;
    }

    public function show(Card $card, User $user) {
    	return $user->cards;
    }
}
