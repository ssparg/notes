<?php

namespace Noter\Http\Controllers;

use Noter\Http\Requests;
use Illuminate\Http\Request;
use Noter\Pad;
use Noter\Card;
use Noter\User;
use Auth;
use Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        $user = User::find($id);
        $pads = Pad::with(['cards.notes'])->orderBy('created_at', 'desc')->get();

        return view('dashboard.home', ['pads' => $pads]);
    }
}
