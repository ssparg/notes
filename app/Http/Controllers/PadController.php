<?php

namespace Noter\Http\Controllers;

use Illuminate\Http\Request;

use Noter\Http\Requests;
use Noter\Pad;
use Auth;
use Response;

class PadController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }

    public function postData(Request $request) {
    	$pads = $request->all();
    	$id = Auth::id();
        
    	foreach ($pads as $singlePad) {
            $pad = new Pad();
    		$pad->user_id = $id;
    		$pad->title = $singlePad['title'];
    		$pad->body = $singlePad['body'];
    		$pad->guid = md5(uniqid() . time());
            
            try {
                $pad->save();
            } catch(\Illuminate\Database\QueryException $e) {
                return Response::json([
                    'result' => FALSE,
                    'message' => 'There was an error saving your card :('
                ], 422);
            }		
    	}

        return Response::json([
            'result' => TRUE,
            'message' => count($pads) > 1 ? 'Your cards have been saved :)' : 'Your card has been saved :)'
        ], 200);   
    }
}
