<?php

namespace Noter;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    public function pad() {
    	return $this->belongsTo(Pad::class);
    }

    public function notes() {
    	return $this->hasMany(Note::class);
    }
}
