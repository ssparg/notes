<?php

namespace Noter;

use Illuminate\Database\Eloquent\Model;

class Pad extends Model
{
    public function user() {
    	return $this->belongsTo(User::class);
    }

    public function cards() {
    	return $this->hasMany(Card::class);
    }
}
