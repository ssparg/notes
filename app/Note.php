<?php

namespace Noter;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
	protected $fillable = ['body'];

    public function card() {
    	return $this->belongsTo('Noter\Card');
    }
}
