var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
	mix.copy('node_modules/font-awesome/fonts/', 'public/fonts/');
	
	mix.sass([
    	'app.scss',
    	'dashboard.scss',
    	'welcome.scss'
    ], 'public/css/welcome.css');
    
    mix.scripts([
    	'add-new-pad.js',
        'config.js'
    ], 'public/js/app.js');
});
